package az.ingress.microservicebootcamp.model;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Car {

    public static void hello(){
        System.out.println("Hello");
    }

    @Override
    protected void finalize() throws Throwable {
        log.info("Car is deleted");
    }
}
