//package az.ingress.microservicebootcamp.controller;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/kafka")
//@Slf4j
//public class PublisherController {
//
//    @Autowired
//    private KafkaTemplate<String, Object> kafkaTemplate;
//
//    @PostMapping("/send")
//    public void publish(@RequestParam String message) {
//        var student = StudentDto.builder()
//                .age(15)
//                .email("ali@mail.ru")
//                .id(1L)
//                .lastName("Mammadov")
//                .build();
//
//        var a = new ProducerRecord<String, Object>("ms-demo-topic", 0, "1243L", student);
//        a.headers().add("Accept-Language", "en".getBytes());
//        kafkaTemplate.send(a);
//    }
//
//}
