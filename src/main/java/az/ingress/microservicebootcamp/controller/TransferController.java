package az.ingress.microservicebootcamp.controller;

import az.ingress.microservicebootcamp.model.Account;
import az.ingress.microservicebootcamp.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@Slf4j
@RequiredArgsConstructor
public class TransferController {

    private final AccountRepository accountRepository;

    @GetMapping
//    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void transfer() {
        log.info("Thread {} started", Thread.currentThread().getId());
        var amount = 40.0;
        var from = accountRepository.findById(8L).get();
        log.info("Thread {} get 'from' account {}", Thread.currentThread().getId(), from);
        if (from.getBalance() < amount) {
            throw new RuntimeException("Not sufficient balance");
        }
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        var to = accountRepository.findById(9L).get();
        log.info("Thread {} get 'to' account {}", Thread.currentThread().getId(), to);
        from.setBalance(from.getBalance() - amount);
        log.info("Thread {} new balance of 'from' account is {}", Thread.currentThread().getId(), from.getBalance());

        to.setBalance(to.getBalance() + amount);
        log.info("Thread {} new balance of 'to' account is {}", Thread.currentThread().getId(), to.getBalance());
        accountRepository.save(from);
        accountRepository.save(to);
        log.info("Thread {} ended", Thread.currentThread().getId());
    }

    @GetMapping("/update")
    @Transactional
    public void update() throws Exception {
        var account = accountRepository.findByName("Ceyhun").get(); //id=8 version=2
        account.setBalance(account.getBalance() + 40);
        log.info("Account balance updated. Going to sleep...");
        Thread.sleep(10000);
        accountRepository.save(account);
        log.info("Account balance updated.");
    }



    @GetMapping("/read")
    @Transactional
    public Account read() throws Exception {
        log.info("trying to read");
        var account = accountRepository.findByName("Ceyhun").get();
        log.info("read success {}", account);
        return account;
    }

}
