package az.ingress.microservicebootcamp.controller;

import az.ingress.microservicebootcamp.model.Account;
import az.ingress.microservicebootcamp.repository.StudentRepository;
import az.ingress.microservicebootcamp.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@Slf4j
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final StudentRepository studentRepository;

    @GetMapping("/1")
    public Account getById() throws Exception {
        return accountService.getById(1L);
    }

    @GetMapping("/2")
    public Account getById2() throws Exception {
        return accountService.getById(2L);
    }

    @GetMapping("/student")
    public void student() {
        System.out.println(studentRepository.findById(1L).get());
    }

}
