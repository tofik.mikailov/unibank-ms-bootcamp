package az.ingress.microservicebootcamp;

import az.ingress.microservicebootcamp.model.PhoneNumber;
import az.ingress.microservicebootcamp.model.Student;
import az.ingress.microservicebootcamp.repository.StudentRepository;
import az.ingress.microservicebootcamp.service.AService;
import az.ingress.microservicebootcamp.service.BService;
import az.ingress.microservicebootcamp.service.CService;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@Slf4j
public class MicroserviceBootcampApplication implements CommandLineRunner {

    private final StudentRepository studentRepository;
    private final CService cService;

    public MicroserviceBootcampApplication(StudentRepository studentRepository,
                                           BService bService) {
        this.studentRepository = studentRepository;
        this.bService = bService;
    }

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceBootcampApplication.class, args);
    }

    @Transactional
    public void method1() {
//        Student.builder()
//                .id(123L)
//                .lastName("Mammadov")
//                .name("Ali")
//                .phoneNumberList(Set.of(
//                        PhoneNumber.builder()
//                                .phone("994555555")
//                                .build(),
//                        PhoneNumber.builder()
//                                .phone("994555555")
//                                .build()))
//                .build();

        studentRepository.findAll();
        studentRepository.findFirstByName("Ali");
    }

    @Transactional
    public void method2() {
        studentRepository.findAll();
        studentRepository.findById(123L);
    }

    @Transactional
    public void method3() {
        studentRepository.findFirstByName("Ali");
        studentRepository.findFirstByName("Ali");
    }

    @Transactional
    public void method4() {
        Student student = studentRepository.findById(123L).get();
        studentRepository.saveAll()
        student.setName("Rahaddin");
        throw new Exception();
    }

    @Override
    public void run(String... args) throws Exception {
        CService cService = new CService(bService);
        cService.print();
    }
}
