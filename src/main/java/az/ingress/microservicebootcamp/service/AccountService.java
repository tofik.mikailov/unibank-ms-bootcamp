package az.ingress.microservicebootcamp.service;

import az.ingress.microservicebootcamp.model.Account;
import az.ingress.microservicebootcamp.repository.AccountRepository;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory emf;

    @SneakyThrows
    public Account getById(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() -> new RuntimeException("account not found"));
//        EntityManager entityManager = emf.createEntityManager();
//        Account account = entityManager.find(Account.class, 1L);
//        entityManager.close();
        Thread.sleep(10);
        return account;
    }
}
