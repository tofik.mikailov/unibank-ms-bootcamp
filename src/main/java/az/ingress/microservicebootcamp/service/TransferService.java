package az.ingress.microservicebootcamp.service;

import az.ingress.microservicebootcamp.model.Account;
import az.ingress.microservicebootcamp.repository.AccountRepository;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;

    public void persistenceContext() throws Exception {
//        var from = accountRepository.findByName("Tofik").get();
//        from.setBalance(100.0);
        var em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        Account account = em.find(Account.class, 1L);
        account.setBalance(100.0);
        em.getTransaction().commit();
        em.close();
    }

    //    @Transactional
    public void cache() {
        entityManager = entityManagerFactory.createEntityManager();
//        Map<Long, Account> cache = new HashMap<>();
//        log.info("get first");

        entityManager.getTransaction().begin();
        Account reference = entityManager.find(Account.class, 8L);
        entityManager.remove(reference);
        reference.setName("Tofik");
        entityManager.flush();
        entityManager.getTransaction().commit();
//        entityManager.find(Account.class, 3L);
//        entityManager.getTransaction().commit();

//        entityManager.getTransaction().begin();
//        entityManager.find(Account.class, 3L);
//        entityManager.getTransaction().commit();
//        accountRepository.findByName("Tofik").get();
//        List<Account> first = accountRepository.findAll();
//        Account first = getAccount(3L, cache);
//        Account first = getAccount(3L, cache);
//        entityManager.close();
//
//        entityManager = entityManagerFactory.createEntityManager();
//        log.info("get second");
//        entityManager.find(Account.class, 3L);
//        accountRepository.findByName("Tofik");
//        List<Account> second = accountRepository.findAll();
//        Account second =  getAccount(3L, cache);
//
//        log.info("get third");
//        entityManager.find(Account.class, 3L);
//        accountRepository.findByName("Tofik");
//        List<Account> third = accountRepository.findAll();
//        Account third =  getAccount(3L, cache);

        //kassa 1
        //find barcode where id = 20; id 20 , count = 100 , version = 123123
        //select , if version == version -> update barcode id =20 ,count = 98, version = 123123

        //kassa 2
        //find barcode where id = 20; id 20 , count = 100 , version = 2233
        //select , if version == version -> update barcode id =20 ,count = 99, version = 123123
        entityManager.close();
    }

    private Account getAccount(Long id, Map<Long, Account> cache) {
        Account account = cache.get(id);
        if (account == null) {
            account = accountRepository.findById(id).get();
            cache.put(account.getId(), account);
        }
        return account;
    }

    public void entityLifeCycles() throws Exception {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        log.info("get reference");
        var acc1 = entityManager.find(Account.class, 3L);
        log.info("print name");
        entityManager.detach(acc1);
        log.info("name is {}", acc1.getName());
        acc1.setName("Hello");
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    public void transferTransactional(Long from, Long to, double amount) throws Exception {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        try {
            Account acc1 = entityManager.find(Account.class, from);
            Account acc2 = entityManager.find(Account.class, to);
            transfer(acc1, acc2, amount);
            entityManager.flush();
        } catch (RuntimeException e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }

    //    @Transactional
    public void transfer(Account from, Account to, double amount) throws Exception {
        log.info("trying to transfer money");
        if (from.getBalance() < amount) {
            throw new RuntimeException("Not sufficient balance");
        }
        from.setBalance(from.getBalance() - amount);
        to.setBalance(to.getBalance() + amount);

        log.info("trying to save one");
        entityManager.persist(from);
        log.info("trying to save two");
        entityManager.persist(to);
    }

    public void transfer3(double amount) throws Exception {
        transfer2(amount);
    }


    public void transfer2(double amount) throws Exception {

    }
}
