package az.ingress.microservicebootcamp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecurityService {

    @Transactional(propagation = Propagation.NESTED)
    public void bye() {

    }
}
