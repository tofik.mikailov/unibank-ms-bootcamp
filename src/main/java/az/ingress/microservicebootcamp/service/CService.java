package az.ingress.microservicebootcamp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CService {

    private AService aService;
    private BService bService;

    public CService(BService bService) {
        this.bService = bService;
    }

    @Autowired
    public void setAService(AService aService) {
        this.aService = aService;
    }

    public void print() {
        bService.hello();
        aService.hello();
    }
}
