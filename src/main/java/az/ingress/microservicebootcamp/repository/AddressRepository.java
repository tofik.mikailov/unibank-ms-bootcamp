package az.ingress.microservicebootcamp.repository;

import az.ingress.microservicebootcamp.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
