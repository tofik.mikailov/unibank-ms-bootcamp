package az.ingress.microservicebootcamp.repository;

import az.ingress.microservicebootcamp.model.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findFirstByName(String name);
}
