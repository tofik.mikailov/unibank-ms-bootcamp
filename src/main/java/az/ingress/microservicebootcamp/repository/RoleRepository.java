package az.ingress.microservicebootcamp.repository;

import az.ingress.microservicebootcamp.model.Address;
import az.ingress.microservicebootcamp.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
