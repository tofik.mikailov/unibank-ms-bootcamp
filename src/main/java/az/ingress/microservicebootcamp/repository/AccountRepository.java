package az.ingress.microservicebootcamp.repository;

import az.ingress.microservicebootcamp.model.Account;
import java.util.List;
import java.util.Optional;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {

    List<Account> findByBalanceGreaterThanAndNameStartsWith(Double balance, String name);

    Optional<Account> findByName(String name);

    @Query(value = "select a.* from account a where a.name = :name", nativeQuery = true)
    Optional<Account> nativeQueryName(String name);

    @Query(value = "select a.* from account a where a.balance = :balance", nativeQuery = true)
    Optional<Account> nativeQueryBalance(Integer balance);

    @Modifying
    @Query(value = "update account set balance = 300", nativeQuery = true)
    Integer updateAccount();
}
